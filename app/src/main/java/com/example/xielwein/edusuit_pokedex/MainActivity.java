package com.example.xielwein.edusuit_pokedex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.xielwein.edusuit_pokedex.adapters.PokemonListAdapter;
import com.example.xielwein.edusuit_pokedex.apis.ApiService;
import com.example.xielwein.edusuit_pokedex.models.Pokemon;
import com.example.xielwein.edusuit_pokedex.models.PokemonRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = "POKEDEX";

  private Retrofit retrofit;

  private RecyclerView recyclerView;
  private PokemonListAdapter listPokemonAdapter;

  private int offset;

  private boolean is_loaded;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);


    recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
    listPokemonAdapter = new PokemonListAdapter(this);
    recyclerView.setAdapter(listPokemonAdapter);
    recyclerView.setHasFixedSize(true);
    final GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
    recyclerView.setLayoutManager(layoutManager);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (dy > 0) {
          int visibleItemCount = layoutManager.getChildCount();
          int totalItemCount = layoutManager.getItemCount();
          int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

          if (is_loaded) {
            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
              Log.i(TAG, "No records to follow");

              is_loaded = false;
              offset += 20;
              getDetails(offset);
            }
          }
        }
      }
    });


    retrofit = new Retrofit.Builder()
        .baseUrl("http://pokeapi.co/api/v2/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    is_loaded = true;
    offset = 0;
    getDetails(offset);
  }

  private void getDetails(int offset) {
    ApiService service = retrofit.create(ApiService.class);
    Call<PokemonRequest> pokemonRequestCall = service.getPokemonList(20, offset);

    pokemonRequestCall.enqueue(new Callback<PokemonRequest>() {
      @Override
      public void onResponse(Call<PokemonRequest> call, Response<PokemonRequest> response) {
        is_loaded = true;
        if (response.isSuccessful()) {

          PokemonRequest pokemonResponse = response.body();
          ArrayList<Pokemon> pokemonList = pokemonResponse.getResults();

          listPokemonAdapter.addPokemonList(pokemonList);

        } else {
          Log.e(TAG, " onResponse: " + response.errorBody());
        }
      }

      @Override
      public void onFailure(Call<PokemonRequest> call, Throwable t) {
        is_loaded = true;
        Log.e(TAG, " onFailure: " + t.getMessage());
      }
    });
  }
}
