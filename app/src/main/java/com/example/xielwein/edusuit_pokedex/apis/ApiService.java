package com.example.xielwein.edusuit_pokedex.apis;

import com.example.xielwein.edusuit_pokedex.models.PokemonRequest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
  @GET("pokemon")
  Call<PokemonRequest> getPokemonList(@Query("limit") int limit, @Query("offset") int offset);
}
