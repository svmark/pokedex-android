package com.example.xielwein.edusuit_pokedex.models;

public class Pokemon {

  private int number;
  private String name;
  private String url;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public int getNumber() {
    String[] url_parts = url.split("/");
    return Integer.parseInt(url_parts[url_parts.length - 1]);
  }

  public void setNumber(int number) {
    this.number = number;
  }
}