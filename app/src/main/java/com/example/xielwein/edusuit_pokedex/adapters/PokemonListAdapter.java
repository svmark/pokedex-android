package com.example.xielwein.edusuit_pokedex.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.xielwein.edusuit_pokedex.DetailsActivity;
import com.example.xielwein.edusuit_pokedex.R;
import com.example.xielwein.edusuit_pokedex.models.Pokemon;

import java.util.ArrayList;


public class PokemonListAdapter extends RecyclerView.Adapter<PokemonListAdapter.ViewHolder> {

  private ArrayList<Pokemon> dataset;
  private Context context;

  public PokemonListAdapter(Context context) {
    this.context = context;
    dataset = new ArrayList<>();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, final int position) {
    final Pokemon p = dataset.get(position);
    holder.nameTextView.setText(p.getName());

    Glide.with(context)
        .load("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getNumber() + ".png")
        .centerCrop()
        .crossFade()
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(holder.pokemonImageView);

    holder.pokemonImageView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String img_url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + p.getNumber() + ".png";

        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("id", p.getNumber());
        intent.putExtra("image", img_url);
        context.startActivity(intent);
      }
    });
  }

  @Override
  public int getItemCount() {
    return dataset.size();
  }

  public void addPokemonList(ArrayList<Pokemon> pokemonList) {
    dataset.addAll(pokemonList);
    notifyDataSetChanged();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    private ImageView pokemonImageView;
    private TextView nameTextView;

    public ViewHolder(View itemView) {
      super(itemView);

      pokemonImageView = (ImageView) itemView.findViewById(R.id.pokemonImageView);
      nameTextView = (TextView) itemView.findViewById(R.id.nameTextView);
    }
  }
}