package com.example.xielwein.edusuit_pokedex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_details);
    final ImageView image = findViewById(R.id.pokemonImageView);
    final TextView txtName = findViewById(R.id.nameTextView);
    final TextView txtHeight = findViewById(R.id.heightTextView);
    final TextView txtWeight = findViewById(R.id.weightTextView);

    RequestQueue queue = Volley.newRequestQueue(this);
    int number = getIntent().getExtras().getInt("id");
    final String image_url = getIntent().getExtras().getString("image");

    String url = "https://pokeapi.co/api/v2/pokemon/" + number;

    JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
        new Response.Listener<JSONObject>() {
          @Override
          public void onResponse(JSONObject response) {
            JSONObject obj = null;

            try {
              obj = new JSONObject(response.toString());
              String name = obj.getString("name");
              String height = obj.getString("height");
              String weight = obj.getString("weight");
              print(image_url);
              Glide.with(DetailsActivity.this)
                  .load(image_url)
                  .centerCrop()
                  .crossFade()
                  .diskCacheStrategy(DiskCacheStrategy.ALL)
                  .into(image);
              txtName.setText(name);
              txtHeight.setText(height);
              txtWeight.setText(weight);

            } catch (JSONException e) {
              e.printStackTrace();
            }
          }
        },
        new Response.ErrorListener() {
          @Override
          public void onErrorResponse(VolleyError error) {
            Log.d("Error.Pokemon", error.getMessage());
          }
        }
    );
    queue.add(getRequest);
  }

  private void print(String message) {
    Log.e("Details Activity: ", message);
  }
}
